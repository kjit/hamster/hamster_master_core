#include "ros/ros.h"

#include "sensor_msgs/CompressedImage.h"

ros::Publisher image_pub;
ros::Subscriber image_sub;

void cameraCallBack(const sensor_msgs::CompressedImage::ConstPtr& msg)
{
    image_pub.publish(msg);
}



int main(int argc, char **argv){
   ros::init(argc, argv, "camera_transfert");
   ros::NodeHandle n;
   
   image_pub = n.advertise<sensor_msgs::CompressedImage>("camera/image/compressed", 1);
   image_sub = n.subscribe("camera/image/compressed_raw", 1, cameraCallBack);
   
   ros::spin();
   
   return 0;
}
   
