/*
 * Filename: ComplementaryFilter.h
 *   Author: Igor Makhtes
 *     Date: Sep 6, 2015
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2015
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SRC_COMPLEMENTARYFILTER_H_
#define SRC_COMPLEMENTARYFILTER_H_


#include <boost/date_time.hpp>

#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <angles/angles.h>
#include <hamster_gps_localization/LocalizationConfig.h>

#include <hamster_gps_localization/LocalizationState.h>


using boost::posix_time::ptime;
using boost::posix_time::time_duration;
using boost::posix_time::microsec_clock;


class ComplementaryFilter {

public:

	/**
	 * Constructs filter
	 * @param alpha The weight of relative measurements
	 */
	ComplementaryFilter(double velocityWeight = 0.95,
			double headingWeight = 0.05, double positionWeight = 0.01);

	virtual ~ComplementaryFilter();

public:

	void addVelocities(double linearX, double linearY, double angularZ);

	void addAbsoluteHeading(double heading);

	void addAbsolutePosition(double x, double y);

	void setState(double x, double y, double heading);

	const LocalizationState& getState() const {
		return state_;
	}

private:

	double velocityWeight_;
	double headingWeight_;
	double positionWeight_;

	bool velocityUpdated_;
	ptime lastVelocityUpdateTime_;

	LocalizationState state_;

	dynamic_reconfigure::Server<hamster_gps_localization::LocalizationConfig> configServer_;

private:

	void configCallback(
			hamster_gps_localization::LocalizationConfig& config, uint32_t level);

};

#endif /* SRC_COMPLEMENTARYFILTER_H_ */
