/*
 * Filename: localization_node.cpp
 *   Author: Igor Makhtes
 *     Date: Sep 6, 2015
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2015
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <ros/ros.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Imu.h>
#include <nav_msgs/Odometry.h>
#include <geodesy/utm.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <hamster_gps_localization/ComplementaryFilter.h>


using namespace std;


ComplementaryFilter* filter_;
bool filterReady_ = false;
geodesy::UTMPoint gpsInitialPose_;
geodesy::UTMPoint gpsLastUtmPose_;
string odomFrame_;
string baseFrame_;
geometry_msgs::Point initialPose_;

ros::Publisher gpsOdomPublisher_;
ros::Publisher odomFilteredPublisher_;


void initializePose(double x, double y, double heading) {
	gpsInitialPose_.easting = gpsLastUtmPose_.easting;
	gpsInitialPose_.northing = gpsLastUtmPose_.northing;

	ROS_INFO("Gps initiali pose set to\n  Global: [ easting = %f, northing = %f ]\n"
			"  Local:  [ x = %f, y = %f ]",
			gpsLastUtmPose_.easting, gpsLastUtmPose_.northing,
			x, y);

	filter_->setState(x, y, heading);

	initialPose_.x = x;
	initialPose_.y = y;
}

void initialPoseCallback(
		const geometry_msgs::PoseWithCovarianceStamped::Ptr& pose) {

	// TODO transform pose to odom frame

	tf::Quaternion q;
	tf::quaternionMsgToTF(pose->pose.pose.orientation, q);

	initializePose(
			pose->pose.pose.position.x,
			pose->pose.pose.position.y,
			tf::getYaw(q));
}

void odomCallback(const nav_msgs::Odometry::Ptr& odom) {

	ROS_INFO_ONCE("Odom message received");

	if (!filterReady_)
		return;

	filter_->addVelocities(
			odom->twist.twist.linear.x,
			odom->twist.twist.linear.y,
			odom->twist.twist.angular.z);
}

void imuCallback(const sensor_msgs::Imu::Ptr& imu) {

	ROS_INFO_ONCE("Imu message received");

	if (!filterReady_)
		return;

	tf::Quaternion q;
	tf::quaternionMsgToTF(imu->orientation, q);

	double heading = tf::getYaw(q);

	filter_->addAbsoluteHeading(heading);
}

void gpsCallback(const sensor_msgs::NavSatFix::Ptr& gps) {

	ROS_INFO_ONCE("Gps message received");

	bool goodGps = (gps->status.status != sensor_msgs::NavSatStatus::STATUS_NO_FIX &&
	                    !std::isnan(gps->altitude) &&
	                    !std::isnan(gps->latitude) &&
	                    !std::isnan(gps->longitude));

	if (!goodGps) {
		ROS_INFO("Gps fix not ready, waiting for fix..");
		return;
	}

	geographic_msgs::GeoPoint lla;
	lla.longitude = gps->longitude;
	lla.latitude = gps->latitude;
	lla.altitude = gps->altitude;

	geodesy::UTMPoint utm(lla);

	gpsLastUtmPose_ = utm;

	if (!filterReady_) {
		initializePose(initialPose_.x, initialPose_.y, 0.0);
		filterReady_ = true;
		return;
	}

	double absolutePoseX = utm.easting + initialPose_.x - gpsInitialPose_.easting;
	double absolutePoseY = utm.northing + initialPose_.y - gpsInitialPose_.northing;

	filter_->addAbsolutePosition(absolutePoseX, absolutePoseY);

	nav_msgs::Odometry msg;

	msg.header.frame_id = odomFrame_;
	msg.header.stamp = ros::Time::now();
	msg.child_frame_id = baseFrame_;

	msg.pose.pose.position.x = absolutePoseX;
	msg.pose.pose.position.y = absolutePoseY;
	msg.pose.pose.orientation.w = 1;

	gpsOdomPublisher_.publish(msg);
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "localization_node");

	ros::NodeHandle node;
	ros::NodeHandle nodePrivate("~");

	double publishRate;

	double velocityWeight;
	double headingWeight;
	double positionWeight;

	double initialPoseX;
	double initialPoseY;

	tf::TransformBroadcaster tfBroadcaster;

	nodePrivate.param("odom_frame", odomFrame_, string("odom"));
	nodePrivate.param("base_frame", baseFrame_, string("base_link"));
	nodePrivate.param("velocity_weight", velocityWeight, 0.9);
	nodePrivate.param("heading_weight", headingWeight, 0.1);
	nodePrivate.param("position_weight", positionWeight, 0.1);

	nodePrivate.param("initial_pose_x", initialPoseX, 0.0);
	nodePrivate.param("initial_pose_y", initialPoseY, 0.0);

	nodePrivate.param("rate", publishRate, 50.0);


	string tfPrefix = tf::getPrefixParam(nodePrivate);
	odomFrame_ = tf::resolve(tfPrefix, odomFrame_);
	baseFrame_ = tf::resolve(tfPrefix, baseFrame_);

	ROS_INFO("Odom frame = %s", odomFrame_.c_str());
	ROS_INFO("Base frame = %s", baseFrame_.c_str());

	filter_ = new ComplementaryFilter(
			velocityWeight, headingWeight, positionWeight);

	initializePose(initialPoseX, initialPoseY, 0.0);

	ros::Subscriber odomSub = node.subscribe("odom", 10, odomCallback);
	ros::Subscriber imuSub = node.subscribe("imu", 10, imuCallback);
	ros::Subscriber gpsSub = node.subscribe("fix", 10, gpsCallback);
	ros::Subscriber initialPoseSub = node.subscribe("initialpose", 1, initialPoseCallback);

	gpsOdomPublisher_ = node.advertise<nav_msgs::Odometry>(
			"odom_gps", 1, false);

	odomFilteredPublisher_ = node.advertise<nav_msgs::Odometry>(
			"odom_filtered", 1, false);

	ros::Rate rate(publishRate);

	nav_msgs::Odometry msg;
	msg.header.frame_id = odomFrame_;
	msg.child_frame_id = baseFrame_;


	while (ros::ok()) {
		tfBroadcaster.sendTransform(
				tf::StampedTransform(
						filter_->getState().position,
						ros::Time::now(),
						odomFrame_,
						baseFrame_));

		msg.header.stamp = ros::Time::now();

		tf::poseTFToMsg(filter_->getState().position, msg.pose.pose);
		odomFilteredPublisher_.publish(msg);

		rate.sleep();
		ros::spinOnce();
	}

	delete filter_;

}
