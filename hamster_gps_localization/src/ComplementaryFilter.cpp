/*
 * Filename: ComplementaryFilter.cpp
 *   Author: Igor Makhtes
 *     Date: Sep 6, 2015
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2015
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include <hamster_gps_localization/ComplementaryFilter.h>


ComplementaryFilter::ComplementaryFilter(double velocityWeight,
		double headingWeight, double positionWeight)
	: velocityWeight_(velocityWeight), headingWeight_(headingWeight),
	  positionWeight_(positionWeight),
	  velocityUpdated_(false), configServer_(ros::NodeHandle("~")) {

	configServer_.setCallback(
			boost::bind(&ComplementaryFilter::configCallback, this, _1, _2));

}

ComplementaryFilter::~ComplementaryFilter() {
}

void ComplementaryFilter::addVelocities(double linearX, double linearY,
		double angularZ) {

	if (!velocityUpdated_) {
		lastVelocityUpdateTime_ = microsec_clock::local_time();
		velocityUpdated_ = true;

		return;
	}
	ptime localTime = microsec_clock::local_time();

	double timeDelta = (localTime - lastVelocityUpdateTime_).total_microseconds() / 1e6;

	double xDelta = velocityWeight_ * linearX * timeDelta;
	double yDelta = velocityWeight_ * linearY * timeDelta;
	double headingDelta = velocityWeight_ * angularZ * timeDelta;

	tf::Transform tfDelta(
			tf::createQuaternionFromYaw(headingDelta),
			tf::Vector3(xDelta, yDelta, 0.0));

	state_.position = state_.position * tfDelta;

	lastVelocityUpdateTime_ = localTime;
}

void ComplementaryFilter::addAbsoluteHeading(double heading) {
	double currentHeading = tf::getYaw(state_.position.getRotation());

	double angleDistance = angles::shortest_angular_distance(currentHeading, heading);

	heading = currentHeading + angleDistance * headingWeight_;

	state_.position.setRotation(tf::createQuaternionFromYaw(heading));
}

void ComplementaryFilter::addAbsolutePosition(double x, double y) {
	x = (1 - positionWeight_) * state_.position.getOrigin().x() + positionWeight_ * x;
	y = (1 - positionWeight_) * state_.position.getOrigin().y() + positionWeight_ * y;

	state_.position.setOrigin(tf::Vector3(x, y, 0.0));
}

void ComplementaryFilter::setState(double x, double y, double heading) {
	state_.position.setOrigin(tf::Vector3(x, y, 0.0));
	state_.position.setRotation(tf::createQuaternionFromYaw(heading));
}

void ComplementaryFilter::configCallback(
		hamster_gps_localization::LocalizationConfig& config, uint32_t level) {
	velocityWeight_ = config.velocity_weight;
	headingWeight_ = config.heading_weight;
	positionWeight_ = config.position_weight;
}
