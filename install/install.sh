#!/bin/bash

PI_USERNAME=pi
PI_HOME_DIR=/home/$PI_USERNAME
PI_ROLE=master # master or slave


if [ $# -eq 0 ]
	then
		echo "No pi role specified, assuming 'master'"
	else 
		PI_ROLE=$1
fi


echo "Pi role = $PI_ROLE"

pushd $(rospack find hamster_launch)


echo "Installing udev rules..."
sudo cp rules/90-rplidar.rules /etc/udev/rules.d/
echo "Done"
echo ""

echo "Installing startup script..."
cp config/hamster-$PI_ROLE-startup $PI_HOME_DIR/hamster-startup
echo "Done"
echo ""

echo "Installing Hamster config file"
cp config/hamster.config $PI_HOME_DIR/hamster.config
echo "Done"
echo ""

echo "Installing cron job (startup)..."
line="@reboot $PI_HOME_DIR/hamster-startup"
(sudo crontab -u $PI_USERNAME -l; echo "$line" ) | sudo crontab -u $PI_USERNAME -
echo "Done"
echo ""

echo "Installing bashrc sourcing"
echo ". ~/hamster.config" >> ~/.bashrc
echo "Done"
echo ""

popd